import React, { useState } from "react";

export default function Day({ numDays }) {
  const dayArr = [...Array(numDays).keys()].map((i) => i + 1);
  const [dayList, setDayList] = useState(dayArr);

  return (
    <div className="col">
      <div className="col__heading">Days</div>
      {dayList.map((dayNum) => {
        return (
          <div className="row" key={dayNum}>
            {dayNum}
          </div>
        );
      })}
    </div>
  );
}
