import React, { useState } from "react";
import Day from "./Day";
import Task from "./Task";

export default function Month() {
  const [curMonth, setCurMonth] = useState({
    monthName: "January",
    numDays: 31,
  });

  const [open, setOpen] = useState(false);

  const months = {
    January: { monthName: "January", numDays: 31 },
    February: { monthName: "February", numDays: 28 },
    March: { monthName: "March", numDays: 31 },
    April: { monthName: "April", numDays: 30 },
    May: { monthName: "May", numDays: 31 },
    June: { monthName: "June", numDays: 30 },
    July: { monthName: "July", numDays: 31 },
    August: { monthName: "August", numDays: 31 },
    September: { monthName: "September", numDays: 30 },
    October: { monthName: "October", numDays: 31 },
    November: { monthName: "November", numDays: 30 },
    December: { monthName: "December", numDays: 31 },
  };

  const activateDropdown = () => {
    setOpen(!open);
  };

  const setDropdownMonth = (e) => {
    setCurMonth(months[e.target.textContent]);
  };

  return (
    <div>
      <h2 className="table__header" onClick={activateDropdown}>
        {curMonth.monthName}
      </h2>
      {/* <div className={`month-dropdown ${open ? "active" : "inactive"}`}>
        <ul>
          <DropdownMonth
            monthName="January"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="February"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="March"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="April"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth monthName="May" setDropdownMonth={setDropdownMonth} />
          <DropdownMonth monthName="June" setDropdownMonth={setDropdownMonth} />
          <DropdownMonth monthName="July" setDropdownMonth={setDropdownMonth} />
          <DropdownMonth
            monthName="August"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="September"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="October"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="November"
            setDropdownMonth={setDropdownMonth}
          />
          <DropdownMonth
            monthName="December"
            setDropdownMonth={setDropdownMonth}
          />
        </ul>
      </div> */}

      <div className="table">
        <div className="data-wrapper">
          <Day numDays={curMonth.numDays} />
          <Task numDays={curMonth.numDays} />
          <div>
            <button>Add Habit</button>
          </div>
        </div>
      </div>
    </div>
  );
}

function DropdownMonth(props) {
  return (
    <li>
      <h2 onClick={props.setDropdownMonth}>{props.monthName}</h2>
    </li>
  );
}
