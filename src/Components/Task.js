import React, { useState } from "react";
import Day from "./Day";

export default function Task({ numDays }) {
  const dayArr = [...Array(numDays).keys()].map((i) => i + 1);
  const [dayList, setDayList] = useState(dayArr);

  return (
    <div className="col">
      <div className="col__heading">TaskName</div>
      {dayList.map((dayNum) => {
        return (
          <div className="row checkbox-container" key={dayNum}>
            <input type="checkbox" id={`cb${dayNum}`} />
            <label htmlFor={`cb${dayNum}`}></label>
          </div>
        );
      })}
    </div>
  );
}
