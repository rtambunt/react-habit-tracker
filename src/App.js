import "./css/App.css";
import Month from "./Components/Month";

function App() {
  return (
    <div className="App">
      <h1>Hey there, Robbie</h1>
      <Month />
    </div>
  );
}

export default App;
